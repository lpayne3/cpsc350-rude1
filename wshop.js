const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');

const app = express();
var config = {
	host: 'localhost',
	user: 'wsmanager',
	password: 'Y0sH1tH3cA7',
	database: 'rude2',
};

var pool = new Pool(config);

app.set('port', (process.env.NODE_PORT));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));
/*
app.get('/api', async (req, res) => {
	var request = req.query;

	console.log(JSON.stringify(req.query));

	if(JSON.stringify(req.query) == '{}'){
		try{
			var response = await pool.query('select distinct workshop from worktable');

			console.log(JSON.stringify(response.rows));
			var shops = [];
			

			for ( var i = 0; i < response.rows.length; i++){
				shops.push(response.rows[i].workshop);
			}
			console.log(shops);
			res.json ({workshops: shops});
		}
		
		catch(e){
			console.error('Error pulling workshops from DB' + e);
		}
	}
	else{
		var workshopReq = req.query;

		var validReq = await pool.query('select workshop from worktable where workshop = $1', [workshopReq.workshop])

		console.log(JSON.stringify(workshopReq));
		try{
			if (validReq.rowCount != 0){
				var response = await pool.query('select attendee from worktable where workshop = $1', [workshopReq.workshop]);
				var attnds = [];

				for(var i = 0; i < response.rows.length; i++){
					attnds.push(response.rows[i].attendee);
				}

				console.log(attnds);
				res.json ({attendees: attnds});
			}else{
				res.json({error: 'workshop not found'});
			}
		}
		catch(e){
			console.error('Error pulling that workshop from the DB' + e);
		}
}
});

*/

app.get('/list-workshops-full', async(req, res) => {
	try{
		var response = await pool.query('select title, date::char(10), location, maxseats, seatsFilled from workshops');

		var detailedList = [];
		for(var i = 0; i < response.rows.length; i++){
			detailedList.push(response.rows[i]);
		}
		res.json({workshops: detailedList});
	}catch(e){
		console.error('error getting detailed workshop information '+e);
	}
});

app.get('/list-workshops', async(req, res) => {
	console.log(JSON.stringify(req.query));

	try{
		var response = await pool.query('select title, date::char(10), location from workshops');

		var workshopList = [];
		for(var i = 0; i < response.rows.length; i++){
			workshopList.push(response.rows[i]);
		}
		console.log(workshopList);
		res.json({workshops: workshopList});
	}catch(e){
		console.error('error pulling workshops from db '+e);
	}
});

app.get('/attendees', async(req, res) => {
	var title = req.query.title;
	var date = req.query.date;
	var loc = req.query.location;

	if(!title || !date || !loc){
		console.log({error: 'get attendees: parameters not given'});
	}
	else{
		try{
			var response = await pool.query('select distinct u.firstname, u.lastname from users u, workshops w, attendees a	WHERE ((u.username = (SELECT a.username where (a.workshopID =(select id from workshops where title=$1 AND date=$2 AND location = $3)))))', [title, date, loc]);

			var attendeeList = [];
			for(var i = 0; i < response.rows.length; i++){
				attendeeList.push(response.rows[i]);	
			}
			console.log(JSON.stringify(response.rows));
			res.json({attendees: attendeeList});
		}catch(e){
			console.error('error on GET attendees '+e);
		}
	}
});

app.get('/list-users', async(req, res) => {
	console.log(req.query);

	var type = req.query;

	if(!req.query.type){
		console.log({error: 'list users: Parameters not given'});
	}
	else{
		try{
			if(req.query.type == 'summary'){
				var response = await pool.query('select firstName, lastName from users');

				var userSummary = [];
				for(var i = 0; i < response.rows.length; i++){
					userSummary.push(response.rows[i]);
				}
				console.log(userSummary);
				res.json({users: userSummary});
			}
			else if(req.query.type == 'full'){
				var response = await pool.query('select firstName, lastName, username, email from users');

				var userFull = [];
				for(var i = 0; i < response.rows.length; i++){
					userFull.push(response.rows[i]);
				}
				console.log(userFull);
				res.json({users: userFull});
			}
			else {
				//type was neither summary nor full
				res.json({error: 'argument type usage: full, summary'});

			}

		}
		catch(e){
			console.error('Error listing users'+e);
		}
	}

});


app.post('/create-user', async(req, res) => {
	console.log(req.body);

	var firstN = req.body.firstname;
	var lastN = req.body.lastname;
	var userN = req.body.username;
	var mail = req.body.email;

	if(!firstN || !lastN || !userN || !mail){
		res.json({error: 'create user: Parameters not given'});
	}
	else{
		try{
			console.log(req.body.username);

			var userDNE = await pool.query('select * from users where username = $1', [userN]);

			console.log(userDNE);
			if(userDNE.rowCount == 0){ //good req, add user to DB
				var response = await pool.query('insert into users (username, firstname, lastname, email) values ($1, $2, $3, $4)', [userN, firstN, lastN, mail]);

				res.json({status: 'user added'});
			} else {
				res.json({error: 'username taken'});
			}
		} catch(e){
			console.log('error adding user to DB', e);
		}
	}

});

app.delete('/drop-class', async(req, res) => {
	var user = req.body.username;
	var title = req.body.title;
	var date = req.body.date;
	var loc = req.body.location;

	if(!user || !title || !date || !loc){
		console.log({error: 'drop class: parameters not given'});
	}
	else{
		try{
			var dropClass = await pool.query('delete from attendees where username=$1 and workshopid = (select id from workshops where title=$2 and date=$3 and location=$4)',[user,title,date,loc]);

			if(dropClass.rowCount == 0){
				res.json({error: 'attendee not found in that workshop'});
			}else{
				res.json({status: 'attendee removed from that workshop'});
			}
		} catch(e){
			console.log('error dropping class ',e);
		}
	}
});

app.delete('/delete-workshop', async(req, res) => {
	var title = req.body.title;
	var date = req.body.date;
	var loc = req.body.location;

	if(!title || !date || !loc){
		console.log({error: 'delete workshop: error with params'});
	}
	else{
		try{
			var dropWShop = await pool.query('delete from workshops where title=$1 and date=$2 and location=$3',[title,date,loc]);
		
			console.log(dropWShop);

			if(dropWShop.rowCount == 0){
				res.json({error: 'workshop not in database'});
			}else{
				res.json({status: 'workshop deleted'});
			}
		
		} catch(e){
			console.log('error deleting workshop ', e);
		}
	}	
});

app.delete('/delete-user', async(req, res) => {
	console.log(req.body);

	var userN = req.body.username;

	if(!userN){
		console.log({error: 'delete user: Parameters not given'});
	}
	else{
		try{
			var userDNE = 
				await pool.query('select * from users where username = $1', [userN]);

			if(userDNE.rowCount != 0){ //user exists, will remove
				var response = await pool.query('delete from users where username = $1', [userN]);

				res.json({status: 'deleted'});
			} else {
				res.json({error: 'that user does not exist'});
			}
		} catch(e){
			console.log('error deleting user from DB', e);
		}
	}
});


app.post('/add-workshop', async(req, res) => {
	console.log(req.body);
	var title = req.body.title;
	var date = req.body.date;
	var loc = req.body.location
	var seatCount = req.body.maxseats;
	var instructor = req.body.instructor;

	if(!title || !date || !loc || !seatCount || !instructor){
		res.json({error: 'parameters not given'});
	}
	else{
		try{
			var workshopDNE = await pool.query('select title, date, location from workshops where title= $1 and date = $2 and location = $3', [title, date, loc]);

			if(workshopDNE.rowCount == 0){
				var response = await pool.query('insert into workshops (title, date, location, maxseats, instructor) values($1,$2,$3,$4,$5)',[title,date,loc,seatCount,instructor]);

				res.json({status: 'workshop added'});
			}
			else{
				res.json({status: 'workshop already in database'});
			}

		} catch(e){
			console.log('error adding workshop to DB'+e);
		}
	}
});

app.post('/enroll', async(req, res) => {
	console.log(req.body);
	var userN = req.body.username;
	var title = req.body.title;
	var date = req.body.date;
	var loc = req.body.location;

	if(!userN || !title || !date || !loc){
		res.json({error: 'parameters not given'});
	}
	else{
		try{
			var updateSeats = await pool.query('UPDATE workshops set seatsFilled = (SELECT count(workshopid) from attendees where workshopid = (select id from workshops where title=$1 and date=$2 and location=$3)) where title=$1 and date=$2 and location=$3',[title,date,loc]);

			var request = await pool.query('select u.username, w.id, w.title, w.date from users u, workshops w where u.username=$1 and w.title=$2 and w.date=$3 and w.location=$4 and (w.seatsFilled+1<=w.maxseats)',[userN,title,date,loc]);

			var checkEnrolled = await pool.query('select * from attendees where username=$1 and workshopid=(select id from workshops where title=$2 and date=$3 and location=$4)',[userN, title, date, loc]);
	
			console.log(request);
			
			if(checkEnrolled.rowCount != 0){
				res.json({status: 'user already enrolled'});
			}

			if(request.rowCount == 0){//then we had a problem with the query
				//check workshop
				var workshopDNE = await pool.query('select * from workshops where title=$1 and date=$2 and location=$3',[title,date,loc]);
				var userDNE = await pool.query('select * from users where username = $1',[userN]);
				var noSeats = await pool.query('select * from workshops where title=$1 and date=$2 and location=$3 and (seatsfilled+1 <= maxseats)', [title,date,loc]);

				if(workshopDNE.rows == 0){
					res.json({status: 'workshop does not exist'});
				} 
				 //else if check user
				else if(userDNE.rows ==0){
					res.json({status: 'user does not exist'});
				}
				//else no seats
				else if(noSeats.rowCount == 0){
					res.json({status: 'no seats available'});
				}
			}// else we are ready to insert our user
			else{
				var response = await pool.query('insert into attendees (username, workshopid) values ((select username from users where username=$1), (select id from workshops where title=$2 and date=$3 and location=$4))', [userN, title, date, loc]);

				var update = await pool.query('UPDATE workshops set seatsFilled = (SELECT count(workshopid) from attendees where workshopid = (select id from workshops where title=$1 and date=$2 and location=$3)) where title=$1 and date=$2 and location=$3',[title,date,loc]);

				res.json({status: 'user enrolled'});

			}


		} catch(e){
			console.log({error: 'error enrolling attendee '+e});
		}
	}			
});


app.post('/api', async(req, res) => {
	console.log(req.body);
	var workshop = req.body.workshop;
	var attendee = req.body.attendee;
	if (!workshop || !attendee){
		res.json({error: 'parameters not given'});
	}
	else {
		try{
			var validReq = await pool.query('select * from worktable where workshop = $1 and attendee = $2', [workshop, attendee]);

			if(validReq.rowCount == 0){
				console.log('Inserting attendee to that workshop');
				//then attendee does not exist in that workshop, create a new entry
				var response = await pool.query('insert into worktable values ($1, $2)', [workshop, attendee]);
				res.json({status: 'user added'});
			}
			else{
				res.json({error: 'Attendee is already enrolled in that workshop'});
			}
		}
		catch(e){
			console.log('Error running insert', e);
		}
	} 
});

app.listen(app.get('port'), () => {
	console.log('Running');
})
