create database workshops;
\c workshops

drop table worktable;

create table worktable(
	workshop text,
	attendee text
);

insert into worktable values('DevOps 101', 'Jack Smith'),
	('DevOps 101', 'Jacob Meldrun'),
	('DevOps 101', 'Josh Rehm'),
	('React Fundamentals', 'Emily Jones'),
	('React Fundamentals', 'John Johnson'),
	('MongoDB', 'Jack Smith'),
	('RESTful', 'Emily Jones'),
	('RESTful', 'Jack Smith');


